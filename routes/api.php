<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registration/step1', 'AuthenticationController@step1');
Route::post('/registration/step2', 'AuthenticationController@step2');
Route::post('/registration', 'AuthenticationController@registration');
Route::post('/login', 'AuthenticationController@login');
Route::post('/forgot_password', 'AuthenticationController@forgot_password');
Route::get('/category_list', 'AuthenticationController@category_list');
Route::get('/fan_statistics/{id}','BidController@fan_statistics');
Route::post('/upload_file','ShoutoutController@upload_file');

Route::group(['middleware' => ['verifyToken']], function () {
    
    Route::post('/change_password', 'AuthenticationController@change_password')->middleware('verifyToken');
    Route::post('/edit_profile', 'AuthenticationController@edit_profile');
    
    Route::post('/idol_list', 'AuthenticationController@idol_list');
    Route::post('/add_bid', 'BidController@add_bid');
    Route::post('/current_bids', 'BidController@current_bids');
    Route::post('/online_status', 'BidController@online_status');
    Route::post('/complete_bid', 'BidController@complete_bid');
    Route::post('/notification_list', 'BidController@notification_list');
    Route::post('/show_status', 'BidController@show_status');
    Route::post('/end_call', 'BidController@end_call');
    Route::post('/logout', 'AuthenticationController@logout');
    Route::post('/statistics','BidController@statistics');
    
    Route::post('/extend_call','BidController@extend_call');
    Route::post('/add_shoutout','ShoutoutController@add_shoutout');
    Route::post('/show_shoutout','ShoutoutController@show_shoutout');
    Route::post('/accept_decline_shoutout','ShoutoutController@accept_decline_shoutout');
    
    Route::post('/save_shoutout_video','ShoutoutController@save_shoutout_video');
    Route::post('/accept_decline_extend_call','BidController@accept_decline_extend_call');
    Route::post('/image_upload','AuthenticationController@upload_image');
    Route::post('/archived','BidController@archived');
    Route::post('/notification','BidController@notification');

    Route::post('/my_shoutout','ShoutoutController@my_shoutout');
    Route::post('/my_request','ShoutoutController@my_request');

    Route::post('/bonus','BidController@bonus');
    Route::post('/referral_list','AuthenticationController@referral_list');
});

