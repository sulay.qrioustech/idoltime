<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('signin');

Route::get('/logout', 'DashboardController@logout')->name('logout');
Route::post('/login', 'DashboardController@login')->name('login');
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('category');
        Route::get('/add', 'CategoryController@add')->name('add.category');
        Route::post('/save', 'CategoryController@save')->name('category.save');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::post('/update', 'CategoryController@update')->name('category.update');
        Route::get('/delete/{id}', 'CategoryController@delete')->name('category.delete');
    });

    Route::group(['prefix' => 'Idol'], function () {
        Route::get('/', 'IdolController@index')->name('idol');
        Route::get('/edit/{id}', 'IdolController@edit')->name('edit.idol');
        Route::post('/update', 'IdolController@update')->name('idol.update');
    });

    Route::group(['prefix' => 'fans'], function () {
        Route::get('/', 'FanController@index')->name('fan');
        Route::get('/edit/{id}', 'FanController@edit')->name('edit.fan');
        Route::post('/update', 'FanController@update')->name('fan.update');
    });
});
