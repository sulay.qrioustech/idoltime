<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallDetails extends Model
{
    //
    public $table = 'video_call_details';

    public function to_details()
    {
        return $this->hasOne(User::class, 'id', 'to_id');
    }
    public function user_details()
    {
        return $this->hasOne(User::class, 'id', 'from_id');
    }
}
