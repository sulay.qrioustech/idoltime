<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Bid extends Model
{
    //
    public $table = 'bid';

    public function fan_details()
    {
        return $this->hasOne(User::class, 'id', 'fan_id');
    }
    public function idol_details()
    {
        return $this->hasOne(User::class, 'id', 'idol_id');
    }
}
