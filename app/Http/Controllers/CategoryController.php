<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;
class CategoryController extends Controller
{
    //
    function index(Request $request)
    {
        $categories = Category::all();
        
        return view('category.list')->with('categories',$categories);
    }

    function add(Request $request)
    {
        return view('category.add');
    }

    function save(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validator->fails())
        {
            return redirect()->route('category')->with('error','Something Went Wrong!!!');
        }
        else
        {
            $category = new Category;
            $category->name = $request->name;
            $category->save();

            return redirect()->route('category')->with('success','Category Added Successfully!!!');
        }
    }

    function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit')->with('category',$category);
    }

    function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'name' => 'required'
        ]);

        if($validator->fails())
        {
            return redirect()->route('category')->with('error','Something Went Wrong!!!');
        }
        else
        {
            $category = Category::find($request->id);
            $category->name = $request->name;
            $category->save();

            return redirect()->route('category')->with('success','Category Updated Successfully!!!');
        }
    }

    function delete($id)
    {
        Category::find($id)->delete();
        return redirect()->route('category')->with('success','Category Deleted Successfully!!!');
    }
}
