<?php

namespace App\Http\Controllers;
use App\Shoutout;
use App\AppNotification;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Library\Notification;
use Nexmo\Laravel\Facade\Nexmo;
use App\Mail\VideoLink;
use Illuminate\Support\Facades\Mail;


class ShoutoutController extends Controller
{
    //
    function add_shoutout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to_id' => 'required',
            'from_id' => 'required',
            'description' => 'required',
            'amount' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
            $user_details = User::find($request->to_id);
            $message = $user_details->fname.''.$user_details->lname." sent you a shoutout request";

            $shoutout = ShoutOut::create($request->all());
            
            $notification = new Notification;
            $notification->send_shoutout_notifcation($user_details->fcm_id,$message,$request->to_id,$request->from_id,$request->description,$request->amount,$request->email,$request->phone,$shoutout->id);
           
          
        
           $appNotification = new AppNotification;
           $appNotification->user_id = $request->to_id;
           $appNotification->message = $message;
           $appNotification->noti_id = 'shoutout-'.$shoutout->id;
           $appNotification->data = json_encode([
                "message" => $message,
                "idol_id" => $request->to_id,
                "fan_id" => $request->from_id,
                "amount" => $request->amount,
                "description" => $request->description,
                "email" => $request->email,
                "phone" => $request->phone,
                "id" =>$shoutout->id,
                "noti_type" => "add_shoutout"
           ]);
           $appNotification->save();
            return ['status' => 1, 'msg' => 'Shoutout sent successfully.'];
        }
    }

    function show_shoutout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idol_id' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
            $result = Shoutout::with('from_details')->where('to_id',$request->idol_id)->where('status',0)->orderBy('id','desc')->get();
            
            if(count($result) > 0)
            {
                return ['status' => 1, 'data' =>$result ,'msg' => 'Success'];
            }
            else
            {
                return ['status' => 0, 'msg' => 'No shoutout request found.'];
            }
        }
    }

    function accept_decline_shoutout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shoutout_id' => 'required',
            'status' => 'required' //1 for accept 2 for reject
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
            $shoutout = Shoutout::with('from_details')->with('to_details')->where('id',$request->shoutout_id)->first();

            $shoutout->status = $request->status;
            $shoutout->save();

            if($request->status == 1)
            {
                $message = $shoutout->to_details->fname.' '.$shoutout->to_details->lname.' has accepted your shoutout request.';
            }
            else
            {
                $message = $shoutout->to_details->fname.' '.$shoutout->to_details->lname.' has decline your shoutout request.';
            }

            $notification = New Notification;
            $notification->send_notifcation($shoutout->from_details->fcm_id,$message);

            $appNotification = new AppNotification;
            $appNotification->user_id = $shoutout->to_id;
            $appNotification->message = $message;
            $appNotification->data = json_encode(['message' => $message]);
            $appNotification->save();

            //new code expire
            $noti = AppNotification::where('noti_id','shoutout-'.$request->shoutout_id)->first();
            $noti->expire = 1;
            $noti->save();
            //end new code

            return ['status' => 1, 'msg' => 'Success'];
            
        }
    }

    function upload_file(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
            $file = $request->file('file');
   
            //Display File Name
            $file_name = $file->getClientOriginalName();
            $file_name = str_replace(' ','_',$file_name);
            $file_name = time().'-'.$file_name;
           
            //Move Uploaded File
            $destinationPath = 'storage/uploads';
            $file->move($destinationPath,$file_name);

            return ['status' => 1, 'data' =>url('/').'/storage/uploads/'.$file_name, 'msg' => 'ok'];
        }
    }

    function save_shoutout_video(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shoutout_id' => 'required',
            'video_link' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
            $shoutout = Shoutout::with('from_details')->find($request->shoutout_id);
            $shoutout->video_link = $request->video_link;
            $shoutout->save();

            $message = $shoutout->to_details->fname.' '.$shoutout->to_details->lname.' has sent you a shoutout video';
            $notification = New Notification;
            $notification->send_notifcation($shoutout->from_details->fcm_id,$message);
            
            Nexmo::message()->send([
                'to'   => $shoutout->from_details->phone,
                'from' => '17819950336',
                'text' => 'Here is your '.$shoutout->description .' video link '. $request->video_link
            ]);
                
            $data = ['message' => 'Here is your '.$shoutout->description .' video link '. $request->video_link ];
            Mail::to($shoutout->email)->send(new VideoLink($data));

            return ['status' => 1, 'msg' => 'Video sent successfully.'];
        }
    }

    function my_shoutout(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
           $shoutouts = Shoutout::with('from_details')->where('to_id',$request->user_id)->get();

            if(count($shoutouts) > 0)
            {
                return ['status' => 1, 'data' => $shoutouts, 'msg' => 'success'];
            }
            else
            {
                return ['status' => 0, 'msg' => 'No Shoutout Found.'];
            }
        }
    }

    function my_request(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 0, 'msg' => 'Please fill all the details.'];
        } else {
           $shoutouts = Shoutout::with('to_details')->where('to_id',$request->user_id)->get();

            if(count($shoutouts) > 0)
            {
                return ['status' => 1, 'data' => $shoutouts, 'msg' => 'success'];
            }
            else
            {
                return ['status' => 0, 'msg' => 'No Shoutout Found.'];
            }
        }
    }
}
