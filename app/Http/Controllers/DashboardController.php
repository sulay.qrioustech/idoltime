<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    //
    function dashboard(Request $request)
    {
        return view('dashboard');
    }

    function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email,'password' => $request->password,'role' => 0]))
        {
            return redirect()->route('dashboard')->with('success','Login Successfully');
        }
        else
        {
            return redirect('/')->with('error','Invalid Credentials');
        }
    }

    function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
