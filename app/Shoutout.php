<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shoutout extends Model
{
    //
    public $table = "shoutout";
    protected $appends = ['name'];

    protected $fillable = [
        'from_id',
        'to_id',
        'description',
        'amount',
        'email',
        'phone'
    ];

    public function to_details()
    {
        return $this->hasOne(User::class, 'id', 'to_id');
    }
    public function from_details()
    {
        return $this->hasOne(User::class, 'id', 'from_id');
    }

    public function getNameAttribute()
    {
        return $this->from_details->fname.' '.$this->from_details->lname;
    }
}
