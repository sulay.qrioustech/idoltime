<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtendCall extends Model
{
    //
    public $table = "extend_call_details";

    protected $fillable = [
        'room_id',
        'minutes',
        'amount'
    ];
}
