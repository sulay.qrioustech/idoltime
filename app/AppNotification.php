<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppNotification extends Model
{
    //
    public $table = 'notification';
    protected $appends = ['message'];
    public function getMessageAttribute()
    {
        return json_decode($this->data);
    }
}
