<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Referral extends Model
{
    //
    public $table = 'referral';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
